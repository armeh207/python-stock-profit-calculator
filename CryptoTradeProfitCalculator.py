# Student name: Yaroslav Makhynia
# Student number: 3001414

# standard imports
import sys
from PyQt5.QtCore import QDate
from PyQt5.QtWidgets import QLabel, QComboBox, QCalendarWidget, QDialog, QApplication, QGridLayout, QSpinBox, QDoubleSpinBox
from PyQt5 import QtCore
from decimal import Decimal

class StockTradeProfitCalculator(QDialog):

    def __init__(self):
        super().__init__()

        # setting up dictionary of stocks
        self.data = self.make_data()
        # sorting the dictionary of stocks by the keys. The keys at the high level are dates, so we are sorting by date
        self.stocks = sorted(self.data.keys())

        # the following 2 lines of code are for debugging purposee and show you how to access the self.data to get dates and prices
        # print all the dates and close prices for BTC
        #print("all the dates and close prices for BTC", self.data['BTC'])
        # print the close price for BTC on 04/29/2013
        #print("the close price for BTC on 04/29/2013",self.data['BTC'][QDate(2013,4,29)])

        # The data in the file is in the following range
        #  first date in dataset - 29th Apr 2013
        #  last date in dataset - 6th Jul 2021
        # When the calendars load we want to ensure that the default dates selected are within the date range above
        #  we can do this by setting variables to store suitable default values for sellCalendar and buyCalendar.
        self.sellCalendarDefaultDate = sorted(self.data['BTC'].keys())[-1] # Accessing the last element of a python list is explained with method 2 on https://www.geeksforgeeks.org/python-how-to-get-the-last-element-of-list/
        #print("self.sellCalendarStartDate",self.sellCalendarDefaultDate)
        self.buyCalendarDefaultDate = sorted(self.data['BTC'].keys())[0]
        #print("self.buyCalendarStartDate", self.buyCalendarDefaultDate)

        # create QLabel for stock purchased
        self.purchasedStockLabel = QLabel("Stock purchased: ")
        self.purchasedStockLabel.setStyleSheet("color: white;")
        # create QComboBox and populate it with a list of stocks
        self.stocksListComboBox = QComboBox()
        self.stocksListComboBox.setStyleSheet("color: white;")
        self.stocksListComboBox.addItems(self.stocks)
        #set the default value to BTC as the dates selected by default are according to BTC
        self.index = self.stocksListComboBox.findText('BTC')
        if self.index >= 0:
            self.stocksListComboBox.setCurrentIndex(self.index)
        # create CalendarWidgets for selection of purchase and sell dates
        self.buyDate=QCalendarWidget()
        #design the calendar
        self.buyDate.setStyleSheet("""color: rgb(87, 85, 85);  
        background-color: black;  
        selection-background-color: rgb(64, 64, 64); 
        selection-color: rgb(0, 255, 0); """)
        #set date range of the calendar according to the range in the dataset
        print(self.stocksListComboBox.currentText())
        #set date range for the calendar
        self.buyDate.setDateRange(self.buyCalendarDefaultDate, self.sellCalendarDefaultDate)
        #two weeks before most recent date
        #set selected date
        self.selectedDefaultBuyDate=QDate(2021,6,22)
        self.buyDate.setSelectedDate(self.selectedDefaultBuyDate)
        self.buyDateLabel=QLabel("Date purchased: ")
        self.buyDateLabel.setStyleSheet("color:white;")
        # this is going to change the background color of the vertical header
        self.buyDate.setVerticalHeaderFormat(QCalendarWidget.NoVerticalHeader)
        self.sellDate=QCalendarWidget()
        self.sellDate.setDateRange(self.buyCalendarDefaultDate, self.sellCalendarDefaultDate)
        # design the calendar
        self.sellDate.setStyleSheet("""color: rgb(87, 85, 85);  
        background-color: black; 
        selection-background-color: rgb(64, 64, 64); 
        selection-color: rgb(0, 255, 0); """)
        self.sellDate.setSelectedDate(self.sellCalendarDefaultDate)
        self.sellDateLabel=QLabel("Date sold: ")
        self.sellDate.setVerticalHeaderFormat(QCalendarWidget.NoVerticalHeader)
        self.sellDateLabel.setStyleSheet("color: white;")
        # create QSpinBox to select stock quantity purchased
        self.quantityPurchasedLabel=QLabel("Quantity purchased: ")
        self.quantityPurchasedLabel.setStyleSheet("color: white;")
        #create QDoubleSpinBox as quantity selected not necessarily is an integer
        self.quantityPurchased = QDoubleSpinBox()
        self.quantityPurchased.setRange(0.00000000001, 9999999)
        #set default value to 1
        self.quantityPurchased.setValue(1)
        self.quantityPurchased.setStyleSheet("color: white;")
        # create QLabels to show the stock purchase total
        self.stockPurchaseTotalLabel=QLabel()
        self.stockPurchaseTotalLabel.setStyleSheet("color: white;")
        # create QLabels to show the stock sell total
        self.stockSellTotalLabel=QLabel("Stocks sell total: ")
        self.stockSellTotalLabel.setStyleSheet("color: white;")
        # create QLabels to show the stock profit total
        self.stockProfitTotalLabel=QLabel("Stock profit total: ")
        self.stockProfitTotalLabel.setStyleSheet("color: white;")
        #set background color to black
        self.setStyleSheet("background-color: black;")

        # initialize the layout - 6 rows to start
        grid = QGridLayout()
        # row 0 - stock selection
        grid.addWidget(self.purchasedStockLabel, 0, 0)
        # row 1 - quantity selection
        grid.addWidget(self.stocksListComboBox, 0, 1)
        grid.addWidget(self.quantityPurchasedLabel,1, 0)
        grid.addWidget(self.quantityPurchased,1,1)
        # row 2 - purchase date selection
        grid.addWidget(self.buyDateLabel,2 ,0)
        grid.addWidget(self.buyDate, 2, 1)
        # row 3 - display purchase total
        grid.addWidget(self.stockPurchaseTotalLabel, 3, 0)
        # row 4 - sell date selection
        grid.addWidget(self.sellDateLabel, 4, 0)
        grid.addWidget(self.sellDate, 4, 1)
        # row 5 - display sell total
        grid.addWidget(self.stockPurchaseTotalLabel, 5, 0)
        # row 6 - display sell total
        grid.addWidget(self.stockSellTotalLabel, 6, 0)
        # row 7 - display profit total
        grid.addWidget(self.stockProfitTotalLabel, 7, 0)

        # set the window title
        self.setWindowTitle("Stock Purchase Profit Calculator")
        # update the UI
        self.updateUi()
        self.setLayout(grid)
        # connecting signals to slots to that a change in one control updates the UI
        self.stocksListComboBox.currentIndexChanged.connect(self.updateCalendar)
        self.stocksListComboBox.currentIndexChanged.connect(self.updateUi)
        self.quantityPurchased.valueChanged.connect(self.updateUi)
        self.buyDate.selectionChanged.connect(self.updateUi)
        self.sellDate.selectionChanged.connect(self.updateUi)
    #function to set selected dates each time selected stock is changed
    def updateCalendar(self):
        try:
            self.buyDate.setSelectedDate(self.sellCalendarDefaultDate.addDays(-14))
            self.sellDate.setSelectedDate(self.sellCalendarDefaultDate)
        except Exception as e:
            print(e)
    def updateUi(self):
        try:
            print("")
            # get selected dates from calendars
            buySelectedDate = self.buyDate.selectedDate()
            sellSelectedDate=self.sellDate.selectedDate()
            # perform necessary calculations to calculate totals
            selectedStockName = str(self.stocksListComboBox.currentText())
            quantityPurchased = self.quantityPurchased.value()
            stockPurchasePrice = self.data[selectedStockName][QDate(buySelectedDate)] * quantityPurchased
            self.stockPurchaseTotalLabel.setText("Stock purchase total: " + "%0.2f" % stockPurchasePrice)
            sellTotalPrice = self.data[selectedStockName][
                                      QDate(sellSelectedDate)] * quantityPurchased
            self.stockSellTotalLabel.setText("Stock sell total: " +"%0.2f" % sellTotalPrice)
            profitMade = sellTotalPrice-stockPurchasePrice
            self.stockProfitTotalLabel.setText("Stock profit total: " +"%0.2f" % profitMade)
            # update the label displaying totals
            # set new ranges for the calendar depending on stock selected as some cryptocurrencies started existing after others
            newCalendarRangeStart=sorted(self.data[selectedStockName].keys())[0]
            newCalendarRangeEnd = sorted(self.data[selectedStockName].keys())[-1]
            self.buyDate.setDateRange(newCalendarRangeStart, newCalendarRangeEnd)
            #set sell date range to stary from the buy date  i. e. you cannot sell the stock before you bought it
            self.sellDate.setDateRange(buySelectedDate, newCalendarRangeEnd)
        except Exception as e:
            print(e)


################ YOU DO NOT HAVE TO EDIT CODE BELOW THIS POINT  ########################################################

    def make_data(self):
        '''
        This code is complete
        Data source is derived from https://www.kaggle.com/camnugent/sandp500/download but use the provided file to avoid confusion

        Converts a CSV file to a dictonary fo dictionaries like

            Stock   -> Date      -> Close
            AAL     -> 08/02/2013 -> 14.75
                    -> 11/02/2013 -> 14.46
                    ...
            AAPL    -> 08/02/2013 -> 67.85
                    -> 11/02/2013 -> 65.56

        Helpful tutorials to understand this
        - https://stackoverflow.com/questions/482410/how-do-i-convert-a-string-to-a-double-in-python
        - nested dictionaries https://stackoverflow.com/questions/16333296/how-do-you-create-nested-dict-in-python
        - https://www.tutorialspoint.com/python3/python_strings.htm
        :return: a dictionary of dictionaries
        '''
        file = open("CryptoCoins_Prices/combined.csv","r")  # open a CSV file for reading https://docs.python.org/3/library/functions.html#open
        data = {}         # empty data dictionary
        file_rows = []    # empty list of file rows
        # add rows to the file_rows list
        for row in file:
            file_rows.append(row.strip()) # https://www.geeksforgeeks.org/python-string-strip-2/
        print("len(file_rows):" + str(len(file_rows)))

        # get the column headings of the CSV file
        row0 = file_rows[0]
        line = row0.split(",")
        column_headings = line
        print(column_headings)

        # get the unique list of stocks from the CSV file
        non_unique_stocks = []
        file_rows_from_row1_to_end = file_rows[1:len(file_rows) - 1]
        for row in file_rows_from_row1_to_end:
            line = row.split(",")
            non_unique_stocks.append(line[6])
        stocks = self.unique(non_unique_stocks)
        print("len(stocks):" + str(len(stocks)))
        print("stocks:" + str(stocks))

        # build the base dictionary of stocks
        for stock in stocks:
            data[stock] = {}

        # build the dictionary of dictionaries
        for row in file_rows_from_row1_to_end:
            line = row.split(",")
            date = self.string_date_into_QDate(line[0])
            stock = line[6]
            close_price = line[4]
            #include error handling code if close price is incorrect
            data[stock][date]= float(close_price)
        print("len(data):",len(data))
        return data


    def string_date_into_QDate(self, date_String):
        '''
        This method is complete
        Converts a data in a string format like that in a CSV file to QDate Objects for use with QCalendarWidget
        :param date_String: data in a string format
        :return:
        '''
        date_list = date_String.split("-")
        date_QDate = QDate(int(date_list[0]), int(date_list[1]), int(date_list[2]))
        return date_QDate


    def unique(self, non_unique_list):
        '''
        This method is complete
        Converts a list of non-unique values into a list of unique values
        Developed from https://www.geeksforgeeks.org/python-get-unique-values-list/
        :param non_unique_list: a list of non-unique values
        :return: a list of unique values
        '''
        # intilize a null list
        unique_list = []

        # traverse for all elements
        for x in non_unique_list:
            # check if exists in unique_list or not
            if x not in unique_list:
                unique_list.append(x)
                # print list
        return unique_list

# This is complete
#if __name__ == '__main__':

app = QApplication(sys.argv)
currency_converter = StockTradeProfitCalculator()
currency_converter.setGeometry(200,200,200,200)
currency_converter.updateUi()
currency_converter.show()
sys.exit(app.exec_())